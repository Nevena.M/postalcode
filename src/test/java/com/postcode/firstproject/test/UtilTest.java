package com.postcode.firstproject.test;

import com.postcode.firstproject.util.Util;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UtilTest {


    @Test
    public void calculateDistanceUtilTest(){

        //prepare
        double latitude1 = 24.03;
        double longitude1 = 67457.05;
        double latitude2 = 24.03;
        double longitude2 = 67457.05;

        //exercise
        double d = Util.calculateDistance(latitude1, longitude1,latitude2, longitude2);

        //verify
        assertEquals(0, d, 0);
    }
}
