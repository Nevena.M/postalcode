package com.postcode.firstproject.test;

import com.postcode.firstproject.entities.Location;
import com.postcode.firstproject.exceptions.PostcodeNotFoundException;
import com.postcode.firstproject.exceptions.PostcodeNotValidException;
import com.postcode.firstproject.model.DistanceModel;
import com.postcode.firstproject.repositories.LocationRepository;
import com.postcode.firstproject.services.LocationServiceImpl;
import com.postcode.firstproject.util.Util;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;


import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class PostalCodeTest {

    @Mock
    private LocationRepository locationRepository;

    @InjectMocks
    private LocationServiceImpl locationServiceImpl;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void getByPostalCodeTest() throws PostcodeNotValidException {

        //prepare
        String postcode = "AB21 7LW";
        Location location1 = new Location("AB21 7LW", 23.67, -1.56);
        when(locationRepository.findByPostcode(postcode)).thenReturn(location1);

        //exercise
        Location location = locationServiceImpl.getByPostalCode(postcode);

        //verify
        assertEquals(location1, location);
    }

    @Test
    public void calculateDistanceTest() throws PostcodeNotValidException {

        //prepare
        Location location1 = new Location("AB21 7LX", 24.56, -2.78);
        Location location2 = new Location("AB21 7LY", 24.56, -2.78);
        when(locationRepository.findByPostcode(location1.getPostcode())).thenReturn(location1);
        when(locationRepository.findByPostcode(location2.getPostcode())).thenReturn(location2);

        //exercise
         DistanceModel distance = locationServiceImpl.calculateDistance(location1.getPostcode(),
                location2.getPostcode());

        //verify
        assertEquals(0, distance.getDistance(), 0);

    }



   @Test
    public void updateLocationTest() throws PostcodeNotValidException {

       //prepare
        Location location1 = new Location("AB21 7LX", 57.34, -2.67);
        Location location2 = new Location("A2010 89", 58.56, -4.56);

        double lat = 57.56;
        double lng = -3.67;

        when(locationRepository.findByPostcode(location1.getPostcode())).thenReturn(location1);

        //exercise
        Location locationUp = locationServiceImpl.updateLocation(location1.getPostcode(), lat, lng);

       //verify
        assertEquals(lat,locationUp.getLatitude(), 0);
        assertEquals(lng, locationUp.getLongitude(), 0);

    }

}