package com.postcode.firstproject.test;

import com.postcode.firstproject.util.Util;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;

import java.util.Arrays;
import java.util.Collection;

@RunWith(value = Parameterized.class)
public class ParameterizedTest {

    private double location1Lat;
    private double location1Lng;
    private double location2Lat;
    private double location2Lng;
    private double expected;

    public ParameterizedTest(double location1Lat, double location1Lng, double location2Lat, double location2Lng, double expected) {
        this.location1Lat = location1Lat;
        this.location1Lng = location1Lng;
        this.location2Lat = location2Lat;
        this.location2Lng = location2Lng;
        this.expected = expected;
    }

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {57.163465880000000, -2.159333430000000, 57.158751170000000, -2.165214861000000, 0.6329384764426358},
                {57.168438400000000,-2.161636000000000, 57.263420000000000,-2.158605000000000, 10.563048603300837},
                {0,0,0,0,0}
        });
    }

    @Test
    public void testCalculateDistance() {
        assertThat(Util.calculateDistance(location1Lat, location1Lng, location2Lat, location2Lng), is(expected));
    }

}