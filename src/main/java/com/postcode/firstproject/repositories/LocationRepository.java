package com.postcode.firstproject.repositories;

import com.postcode.firstproject.entities.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationRepository extends JpaRepository<Location, Long> {

    Location findByPostcode(String postcode);

    /*@Query("select l from Location l where l.postcode = :postcode and l.latitude = :latitude ")
    Location findByPostcodeAndLatitude(@Param("postcode") String postcode, @Param("latitude") String latitude);*/

}
