package com.postcode.firstproject.controllers;


import com.postcode.firstproject.entities.Location;
import com.postcode.firstproject.exceptions.PostcodeNotFoundException;
import com.postcode.firstproject.exceptions.PostcodeNotValidException;
import com.postcode.firstproject.model.DistanceModel;
import com.postcode.firstproject.repositories.LocationRepository;
import com.postcode.firstproject.services.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class LocationResource {


    @Autowired
    private LocationService locationService;


    @PostMapping("/postcode")

    public ResponseEntity<Object> getByPostCode(@RequestBody String postCode) throws PostcodeNotValidException {
        ResponseEntity<Object> re = null;
        try{
            re = ResponseEntity.ok(locationService.getByPostalCode(postCode));
        }catch (PostcodeNotValidException ex){
            ex.getMessage();
        }
        return re;
    }

    @PostMapping("/distance")
    public DistanceModel calculateDistance(@ModelAttribute(name = "dist1")String postCode1,
                                           @ModelAttribute(name="dist2") String postCode2) throws PostcodeNotValidException {

        return locationService.calculateDistance(postCode1, postCode2);


    }

    @PutMapping("/update")
    public Location updateLocation(@ModelAttribute (name="postCode") String postCode,
                                   @ModelAttribute (name="longi") double longi,
                                   @ModelAttribute (name="latit") double latit) throws PostcodeNotValidException {

        return locationService.updateLocation(postCode, longi, latit);


    }


}
