package com.postcode.firstproject.restExceptionHandler;

import com.postcode.firstproject.exceptions.PostcodeNotFoundException;
import com.postcode.firstproject.exceptions.PostcodeNotValidException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value
            = { PostcodeNotFoundException.class, PostcodeNotValidException.class })
    protected ResponseEntity<Object> handleConflict(
            RuntimeException ex, final HttpStatus status, WebRequest request) {
        String bodyOfResponse = "This should be application specific";
        ex.getMessage();
        return new ResponseEntity<>(bodyOfResponse,status);
    }
}
