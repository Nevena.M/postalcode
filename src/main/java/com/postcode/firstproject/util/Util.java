package com.postcode.firstproject.util;


import com.postcode.firstproject.exceptions.PostcodeNotValidException;

import java.util.regex.Pattern;

public class Util {

    private static final String postCode = "^[A-Z]{1,2}[0-9R][0-9A-Z]? [0-9][ABD-HJLNP-UW-Z]{2}$";
    private static Pattern patternP = Pattern.compile(postCode);

    private final static double EARTH_RADIUS = 6371;
    //radius in kilometers


    public static double calculateDistance(double latitude, double longitude,
                                     double latitude2, double longitude2) {
        // Using Haversine formula! See Wikipedia;
        double lon1Radians = Math.toRadians(longitude);
        double lon2Radians = Math.toRadians(longitude2);
        double lat1Radians = Math.toRadians(latitude);
        double lat2Radians = Math.toRadians(latitude2);
        double a = haversine(lat1Radians, lat2Radians) + Math.cos(lat1Radians) *Math.cos(lat2Radians) *haversine(lon1Radians, lon2Radians);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));return (EARTH_RADIUS * c);
    }



    private static double haversine(double deg1, double deg2) {

        return square(Math.sin((deg1 - deg2) / 2.0));

    }

    private static double square(double x) {
    return x * x;
}


    public static boolean isValidCheck(String postCode){
        return patternP.matcher(postCode).find();
    }

    /*public static boolean isValidPostcode(String postCode)throws PostcodeNotValidException{

        if(!isValidCheck(postCode) || postCode.isEmpty() || (postCode == null))
            throw new PostcodeNotValidException();
        else
           return true;

    }*/


}
