package com.postcode.firstproject.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class PostcodeNotFoundException extends Exception {

    public PostcodeNotFoundException() {
    }

    public PostcodeNotFoundException(String message) {
        super(message);
    }
}
