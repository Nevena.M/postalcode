package com.postcode.firstproject.exceptions;


public class PostcodeNotValidException extends PostcodeNotFoundException {

    public PostcodeNotValidException() {
    }

    public PostcodeNotValidException(String message) {

        message = new String("Postcode is not valid");
    }
}
