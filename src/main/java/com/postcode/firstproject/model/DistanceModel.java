package com.postcode.firstproject.model;


import org.springframework.context.annotation.ComponentScan;

@ComponentScan
public class DistanceModel {


    private String location1;
    private String location2;
    private double distance;
    String unit = "km";

    public DistanceModel() {
    }

    public DistanceModel(String location1, String location2, double distance, String unit) {
        this.location1 = location1;
        this.location2 = location2;
        this.distance = distance;
        this.unit = unit;
    }


    public String getLocation1() {
        return location1;
    }

    public void setLocation1(String location1) {
        this.location1 = location1;
    }

    public String getLocation2() {
        return location2;
    }

    public void setLocation2(String location2) {
        this.location2 = location2;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
