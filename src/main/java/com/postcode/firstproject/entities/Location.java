package com.postcode.firstproject.entities;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name= "postcodelatlng")
public class Location {

  @Id
  @GeneratedValue
  @Column(name="id")
  private long id;

  @Column(name="postcode")
  private String postcode;

  @Column(name="latitude")
  private Double latitude;

  @Column(name="longitude")
  private Double longitude;

  public Location() {
  }

  public Location(String postcode, Double latitude, Double longitude) {
    this.postcode = postcode;
    this.latitude = latitude;
    this.longitude = longitude;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getPostcode() {
    return postcode;
  }

  public void setPostcode(String postcode) {
    this.postcode = postcode;
  }

  public Double getLatitude() {
    return latitude;
  }

  public void setLatitude(Double latitude) {
    this.latitude = latitude;
  }

  public Double getLongitude() {
    return longitude;
  }

  public void setLongitude(Double longitude) {
    this.longitude = longitude;
  }

  @Override
  public String toString() {
    return "Location{" +
            " postcode='" + postcode + '\'' +
            ", latitude=" + Math.toRadians(latitude) +
            ", longitude=" + Math.toRadians(longitude) +
            '}';
  }
}
