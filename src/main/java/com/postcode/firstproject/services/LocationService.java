package com.postcode.firstproject.services;

import com.postcode.firstproject.entities.Location;
import com.postcode.firstproject.exceptions.PostcodeNotFoundException;
import com.postcode.firstproject.exceptions.PostcodeNotValidException;
import com.postcode.firstproject.model.DistanceModel;


public interface LocationService {

    Location getByPostalCode(String postalCode) throws PostcodeNotValidException;
    DistanceModel calculateDistance(String postcode1, String postcode2) throws PostcodeNotValidException;
    Location updateLocation(String postCode, double d1, double d2) throws PostcodeNotValidException;

}
