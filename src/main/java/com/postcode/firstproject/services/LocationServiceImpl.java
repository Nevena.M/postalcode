package com.postcode.firstproject.services;

import com.postcode.firstproject.entities.Location;
import com.postcode.firstproject.exceptions.PostcodeNotFoundException;
import com.postcode.firstproject.exceptions.PostcodeNotValidException;
import com.postcode.firstproject.model.DistanceModel;
import com.postcode.firstproject.repositories.LocationRepository;
import com.postcode.firstproject.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class LocationServiceImpl implements LocationService {

    @Autowired
    private LocationRepository locationRepository;


    @Override
    @Transactional
    public Location getByPostalCode(String postCode) throws PostcodeNotValidException {

        Location location = null;

        if (Util.isValidCheck(postCode)) {
            location = locationRepository.findByPostcode(postCode);
        }
        else
            throw new PostcodeNotValidException("Nije validno");

        return location;

    }


    @Override
    @Transactional
    public DistanceModel calculateDistance(String postcode1, String postcode2) throws PostcodeNotValidException {

        Location location1 = getByPostalCode(postcode1);

        Location location2 = getByPostalCode(postcode2);

        double distance =  Util.calculateDistance(location1.getLatitude(), location1.getLongitude(),
                location2.getLatitude(), location2.getLongitude());

        DistanceModel distanceModel = new DistanceModel(location1.toString(),location2.toString(), distance, "km");

        return distanceModel;
    }

    @Override
    @Transactional
    public Location updateLocation(String postCode, double d1, double d2) throws PostcodeNotValidException {

        Location location = getByPostalCode(postCode);


        location.setLatitude(d1);
        location.setLongitude(d2);

        return location;
    }


}
